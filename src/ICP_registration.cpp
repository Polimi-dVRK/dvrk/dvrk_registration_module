/* ROS node with ITK implementation for Iterative closest point. C++ code by Anna Morelli and
ROS implementation by Hirenkumar Nakawala */

/* INPUT: 2 *.txt files:
	intraPointsFile --> coordinates of intra points
	prePointsFile --> coordinates of pre-operative points

Point coordinates have to be separated by single space. For example:

	p0x p0y p0z
	p1x p1y p1z
	...

Output: ICPexitCondition.txt --> information about residual error and number of iterations
	registredPoint.fcsv --> files with the pre-point new (registered coordinates)

*/
/* ROS headers */

#include "ros/ros.h"
#include "std_msgs/String.h"
// custom ROS msg to save transformation matrix as a 6 elements array i.e. 6 elements of transformation matrix
#include "registration_module/transform.h"

/*ITK headers */
 
#include "itkEuler3DTransform.h"
#include "itkEuclideanDistancePointMetric.h"
#include "itkLevenbergMarquardtOptimizer.h"
#include "itkPointSetToPointSetRegistrationMethod.h"
#include <iostream>
#include <fstream>
#include "itkTransformFileWriter.h"
#include "itkPointSet.h"
#include "itkOptimizerParameters.h"
using namespace std;

class CommandIterationUpdate : public itk::Command
{
public:
  typedef CommandIterationUpdate  Self;
  typedef itk::Command            Superclass;
  typedef itk::SmartPointer<Self> Pointer;
  itkNewMacro( Self );
protected:
  CommandIterationUpdate() {};
public:
  typedef itk::LevenbergMarquardtOptimizer     OptimizerType;
  typedef const OptimizerType *                OptimizerPointer;
  void Execute(itk::Object *caller, const itk::EventObject & event) ITK_OVERRIDE
    {
    Execute( (const itk::Object *)caller, event);
    }
  void Execute(const itk::Object * object, const itk::EventObject & event) ITK_OVERRIDE
    {
    OptimizerPointer optimizer = dynamic_cast< OptimizerPointer >( object );
    if( optimizer == ITK_NULLPTR )
      {
      itkExceptionMacro( "Could not cast optimizer." );
      }
    if( ! itk::IterationEvent().CheckEvent( &event ) )
      {
      return;
      }
    //cout << "Value = " << optimizer->GetCachedValue() << endl;
    //cout << "Position = "  << optimizer->GetCachedCurrentPosition()<<endl;
    }
};


int main(int argc, char * argv[] )
{
  if( argc < 3 )
    {
    cerr << "Arguments Missing. " << endl;
    cerr << "Usage:  registration_module_node intraPointsFile prePointsFile"<< endl;
    return EXIT_FAILURE;
    }

  ros::init(argc, argv, "ICP_registration");

  ros::NodeHandle n;

  const unsigned int Dimension = 3;

// 1) Define the types for the pre and intra point sets.

  typedef itk::PointSet< float, Dimension >   PointSetType;
  PointSetType::Pointer intraPointSet  = PointSetType::New();
  PointSetType::Pointer prePointSet = PointSetType::New();
  typedef PointSetType::PointType     PointType;
  typedef PointSetType::PointsContainer  PointsContainer;
  PointsContainer::Pointer intraPointContainer  = PointsContainer::New();
  PointsContainer::Pointer prePointContainer = PointsContainer::New();
  PointType intraPoint;
  PointType prePoint;
  PointType registeredPoint;

  typedef double ScalarType;

// 2) Read the input files containing the coordinates of intra and pre points. - Get intra-operative from GUI - subscriber missing
  ifstream   intraFile;
  intraFile.open( argv[1] );
  if( intraFile.fail() )
    {
    cerr << "Error opening points file with name : " << endl;
    cerr << argv[1] << endl;
    return EXIT_FAILURE;
    }
  unsigned int pointId = 0;
  intraFile >> intraPoint;
  while( !intraFile.eof() )
    {
    intraPointContainer->InsertElement( pointId, intraPoint );
    intraFile >> intraPoint;
    pointId++;
    }
  intraPointSet->SetPoints( intraPointContainer );
  //cout << "Number of intra Points = " << intraPointSet->GetNumberOfPoints()<< endl;
    
  
  ifstream   preFile;
  preFile.open( argv[2] );
  if( preFile.fail() )
    {
    cerr << "Error opening points file with name : " << endl;
    cerr << argv[2] << endl;
    return EXIT_FAILURE;
    }
  pointId = 0;
  preFile >> prePoint;
  while( !preFile.eof() )
    {
    prePointContainer->InsertElement( pointId, prePoint );
    preFile >> prePoint;
    pointId++;
    }
  prePointSet->SetPoints( prePointContainer );
 // cout << "Number of pre Points = " << prePointSet->GetNumberOfPoints() << endl;

// 3) Setup the metric, the transform, the optimizer and the regitration

  typedef itk::EuclideanDistancePointMetric<
                                    PointSetType,
                                    PointSetType>
                                                    MetricType;
  MetricType::Pointer  metric = MetricType::New();


  typedef itk::Euler3DTransform< double >      TransformType;
  TransformType::Pointer transform = TransformType::New();

  typedef itk::LevenbergMarquardtOptimizer OptimizerType;
  OptimizerType::Pointer      optimizer     = OptimizerType::New();
  optimizer->SetUseCostFunctionGradient(false);

  typedef itk::PointSetToPointSetRegistrationMethod<
                                            PointSetType,
                                            PointSetType >
                                                    RegistrationType;
  RegistrationType::Pointer   registration  = RegistrationType::New();

  // Scale the translation components of the Transform in the Optimizer
  OptimizerType::ScalesType scales( transform->GetNumberOfParameters() );

// 4) Set scales and ranges for translations and rotations in the transform, as well as convergence criteria and number of iteration.
    
  scales.Fill(1);
  unsigned long   numberOfIterations =  150; 
  double          gradientTolerance  =  1e-17;   // convergence criterion
  double          valueTolerance     =  1e-17;   // convergence criterion
  double          epsilonFunction    =  1e-17;   // convergence criterion
  optimizer->SetScales( scales );
  optimizer->SetNumberOfIterations( numberOfIterations );
  optimizer->SetValueTolerance( valueTolerance );
  optimizer->SetGradientTolerance( gradientTolerance );
  optimizer->SetEpsilonFunction( epsilonFunction );

// 5) Inititialize the transform as an identity

  transform->SetIdentity();
  registration->SetInitialTransformParameters( transform->GetParameters() );


// 6) Connect all the components required for the registration

  registration->SetMetric(        metric        );
  registration->SetOptimizer(     optimizer     );
  registration->SetTransform(     transform     );
  registration->SetFixedPointSet( intraPointSet );
  registration->SetMovingPointSet(   prePointSet   );
  CommandIterationUpdate::Pointer observer = CommandIterationUpdate::New();
  optimizer->AddObserver( itk::IterationEvent(), observer );
  
// 7) Compute registration-> Get Transformation

  try
    {
    registration->Update();
    }
  catch( itk::ExceptionObject & e )
    {
    cerr << e << endl;
    return EXIT_FAILURE;
    }
  cout << "ICP exit conditiona and solution = " << transform->GetParameters() << endl;
  ofstream myfileExit;
  myfileExit.open("ICPexitCondition.txt");
  
  ros::Publisher transformation_pub = n.advertise<registration_module::transform>("transformation", 1000);
  ros::Rate loop_rate(10);

  myfileExit << "Stopping condition: " << optimizer->GetStopConditionDescription() << endl;
  myfileExit.close();

    ofstream myfile;
    myfile.open("registeredPoints.fcsv");

    typedef PointsContainer::Iterator     PointsIterator;

    PointsIterator  pointIterator = prePointContainer->Begin();
    PointsIterator end = prePointContainer->End();

    while (pointIterator != end)
    {
        PointType p = pointIterator.Value();
        registeredPoint = transform->TransformPoint( p );
        myfile<< "1,"<< registeredPoint[0]<<","<< registeredPoint[1]<<","<< registeredPoint[2] <<"\n";
        //the 1 is added for Slicer's purpose
        ++pointIterator;
    }

    myfile.close();

  int count = 0;
  while (ros::ok())
  {
	registration_module::transform msg;
      const boost::array<double, 3ul> newtransform = {{registeredPoint[0], registeredPoint[1], registeredPoint[2]}};
	msg.data = newtransform;

	transformation_pub.publish(msg);

	ros::spinOnce ();
	loop_rate.sleep();
	++count;
  }

  return 0;
} 



