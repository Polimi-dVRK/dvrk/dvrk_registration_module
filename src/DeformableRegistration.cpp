//DEFORMABILE - Free Form Deformation based on B-splines


/* INPUT: 2 *.txt files:
intraPointsFile     --> coordinates of intra points
prePointsFile    --> coordinates of pre points

Point coordinates have to be separated by single space. Example:
p0x p0y p0z
p1x p1y p1z
...

OUTPUT:
DeformableExitCondition.txt  --> information about residual error and number of iterations
registeredPoint.fcsv       --> files with the pre-point new (registered) coordinates

*/

//ROS headers

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "registration_module/transform.h"


// include the relevant headers.
//
#include "itkEuclideanDistancePointMetric.h"
#include "itkLevenbergMarquardtOptimizer.h"
#include "itkPointSetToPointSetRegistrationMethod.h"
#include <iostream>
#include <fstream>
#include "itkBSplineTransform.h"
#include "itkTransformFileWriter.h"
using namespace std;


#include "itkCommand.h"
const unsigned int Dimension = 3;



class CommandIterationUpdate : public itk::Command
{
public:
  typedef CommandIterationUpdate  Self;
  typedef itk::Command            Superclass;
  typedef itk::SmartPointer<Self> Pointer;
  itkNewMacro( Self );
protected:
  CommandIterationUpdate() {};
public:
  typedef itk::LevenbergMarquardtOptimizer     OptimizerType;
  typedef const OptimizerType *                OptimizerPointer;
  
  void Execute(itk::Object *caller, const itk::EventObject & event) ITK_OVERRIDE
    {
    Execute( (const itk::Object *)caller, event);
    }
  void Execute(const itk::Object * object, const itk::EventObject & event) ITK_OVERRIDE
    {
    OptimizerPointer optimizer = dynamic_cast< OptimizerPointer >( object );
    if( optimizer == ITK_NULLPTR )
      {
      itkExceptionMacro( "Could not cast optimizer." );
      }
    if( ! itk::IterationEvent().CheckEvent( &event ) )
      {
      return;
      }

    }
}; 




int main(int argc, char * argv[] )
{
  unsigned long   numberOfIterations = 1; // convergence criterion
  double          gradientTolerance = 1e-5000;   // convergence criterion
  double          valueTolerance = 1e-5000;   // convergence criterion
  double          epsilonFunction = 1e-5000;   // convergence criterion
  int mesh_size_x;
  int mesh_size_y;
  int mesh_size_z;

  if( argc < 3 )
    {
    std::cerr << "Arguments Missing. " << std::endl;
    std::cerr <<
      "Usage:  Bspline   intraPointsFile  prePointsFile "
      << std::endl;
    return EXIT_FAILURE;
    }

  ros::init(argc, argv, "ICP_registration");

  ros::NodeHandle n;

// 1) Define the necessary types for the pre and intra point sets.

  typedef itk::PointSet< float, Dimension >   PointSetType;
  PointSetType::Pointer intraPointSet  = PointSetType::New();
  PointSetType::Pointer prePointSet = PointSetType::New();
  typedef PointSetType::PointType     PointType;
  typedef PointSetType::PointsContainer  PointsContainer;
  PointsContainer::Pointer intraPointContainer  = PointsContainer::New();
  PointsContainer::Pointer prePointContainer = PointsContainer::New();
  PointType intraPoint;
  PointType prePoint;
  PointType registeredPoint;


  typedef double ScalarType;

// 2) Read the file containing coordinates of intra points.
  ifstream   intraFile;
  intraFile.open( argv[1] );
  if(intraFile.fail() )
    {
    cerr << "Error opening points file with name : " << endl;
    cerr << argv[1] << endl;
    return EXIT_FAILURE;
    }


  float max_x;
  float min_x;
  float max_y;
  float min_y;
  float max_z;
  float min_z;
  float temp_x;
  float temp_y;
  float temp_z;
  unsigned int pointId = 0;
  intraPointContainer->InsertElement(pointId, intraPoint);
  intraFile >> intraPoint;
  max_x = intraPoint[0];
  min_x = intraPoint[0];
  max_y = intraPoint[1];
  min_y = intraPoint[1];
  max_z = intraPoint[2];
  min_z = intraPoint[2];
  temp_x = intraPoint[0];
  temp_y = intraPoint[1];
  temp_z = intraPoint[2];
  pointId = 1;

 

  while( !intraFile.eof() )
    {
	  intraPointContainer->InsertElement( pointId, intraPoint );
	  intraFile >> intraPoint;

    if (intraPoint[0]>max_x)
    	max_x = intraPoint[0];
	if (intraPoint[0]<min_x)
		min_x = intraPoint[0];
	if (intraPoint[1]>max_y)
    	max_y = intraPoint[1];
	if (intraPoint[1]<min_y)
		min_y = intraPoint[1];
    if (intraPoint[2]>max_z)
    	max_z = intraPoint[2];
	if (intraPoint[2]<min_z)
		min_z = intraPoint[2];

    temp_x = temp_x + intraPoint[0];
    temp_y = temp_y + intraPoint[1];
    temp_z = temp_z + intraPoint[2];

    pointId++;
    }

  intraPointSet->SetPoints(intraPointContainer );


  float o_x, o_y;
  float o_z;
  o_x = temp_x/(intraPointSet->GetNumberOfPoints());
  o_y = temp_y/(intraPointSet->GetNumberOfPoints());
  o_z = temp_z/(intraPointSet->GetNumberOfPoints());
  
  float dim_x, dim_y;
  float dim_z;
  dim_x = abs(max_x-min_x);
  dim_y = abs(max_y-min_y);
  dim_z = abs(max_z-min_z);
  mesh_size_x = dim_x / 15;
  mesh_size_y = dim_y / 15;
  mesh_size_z = dim_z / 15;
    
  
 	
  // Read the file containing coordinates of pre points.
  ifstream   preFile;
  preFile.open( argv[2] );
  if(preFile.fail() )
    {
    std::cerr << "Error opening points file with name : " << std::endl;
    std::cerr << argv[2] << std::endl;
    return EXIT_FAILURE;
    }
  pointId = 0;

  preFile >> prePoint;
  while( !preFile.eof() )
    {
	  prePointContainer->InsertElement( pointId, prePoint );
	  preFile >> prePoint;

    pointId++;
    }

  prePointSet->SetPoints(prePointContainer );


// 3) Setup the metric, the tranform, optimizers, and registration.

  typedef itk::EuclideanDistancePointMetric<
                                    PointSetType,
                                    PointSetType>
                                                    MetricType;
  MetricType::Pointer  metric = MetricType::New();

  const unsigned int SpaceDimension = 3;
  const unsigned int SplineOrder = 3;
  typedef double CoordinateRepType;

  typedef itk::BSplineTransform<
                            CoordinateRepType,
                            SpaceDimension,
                            SplineOrder >     TransformType;
  TransformType::Pointer transform = TransformType::New();

  itk::BSplineTransform<
                            CoordinateRepType,
                            SpaceDimension,
                            SplineOrder >::OriginType origin;
  origin[0] = o_x;
  origin[1] = o_y;
  origin[2] = o_z;

  itk::BSplineTransform<
                            CoordinateRepType,
                            SpaceDimension,
                            SplineOrder >::PhysicalDimensionsType physicalDimensions; 
  physicalDimensions[0] = dim_x;
  physicalDimensions[1] = dim_y;
  physicalDimensions[2] = dim_z;

  itk::BSplineTransform<
                            CoordinateRepType,
                            SpaceDimension,
                            SplineOrder >::MeshSizeType meshSize;  //uniform wrt physicalDimensions
  meshSize[0] = mesh_size_x;
  meshSize[1] = mesh_size_y;
  meshSize[2] = mesh_size_z;

  transform->SetTransformDomainOrigin( origin );
  transform->SetTransformDomainPhysicalDimensions( physicalDimensions );
  transform->SetTransformDomainMeshSize( meshSize );

 
  typedef itk::LevenbergMarquardtOptimizer OptimizerType;
    OptimizerType::Pointer      optimizer     = OptimizerType::New();
  optimizer->SetUseCostFunctionGradient(false);

  typedef itk::PointSetToPointSetRegistrationMethod<
                                            PointSetType,
                                            PointSetType >
                                                    RegistrationType;
  RegistrationType::Pointer   registration  = RegistrationType::New();


  // Scale the translation components of the Transform in the Optimizer


  OptimizerType::ScalesType scales( transform->GetNumberOfParameters() );

  
  

  scales.Fill(1.0);
  optimizer->SetScales( scales );
  optimizer->SetNumberOfIterations( numberOfIterations );
  optimizer->SetValueTolerance( valueTolerance );
  optimizer->SetGradientTolerance( gradientTolerance );
  optimizer->SetEpsilonFunction( epsilonFunction );





// 5) Inititialize the transform as an identity.

  transform->SetIdentity();
  //cout << "Transform initial parameters: " << transform->GetParameters() << endl;
  registration->SetInitialTransformParameters( transform->GetParameters() );


// 6) Connect all the components required for the registration.

  registration->SetMetric(        metric        );
  registration->SetOptimizer(     optimizer     );
  registration->SetTransform(     transform     );
  registration->SetFixedPointSet(	intraPointSet );
  registration->SetMovingPointSet(	prePointSet   );
  CommandIterationUpdate::Pointer observer = CommandIterationUpdate::New();
  optimizer->AddObserver( itk::IterationEvent(), observer );



  
  try
    {
    registration->Update();
	     }
  catch( itk::ExceptionObject & e )
    {
    std::cerr << e << std::endl;
    return EXIT_FAILURE;
    }
  
  // write the stopping conditions on a txt file
  ofstream myfileExit;
  myfileExit.open("DeformableExitCondition.txt");
  myfileExit << "Stopping condition: " << optimizer->GetStopConditionDescription() << endl;
  myfileExit.close();


// 8) Apply the obtained transformation and write the registered point on a file
  
  
  ofstream myfile;
  myfile.open("registeredPoints_def.fcsv");

  typedef PointsContainer::Iterator     PointsIterator;
  PointsIterator  pointIterator = prePointContainer->Begin();
  PointsIterator end = prePointContainer->End();
  while (pointIterator != end)
  {
	  PointType p = pointIterator.Value();   
	  registeredPoint = transform->TransformPoint(p);
	  myfile << "1," << registeredPoint[0] << "," << registeredPoint[1] << "," << registeredPoint[2] << "\n";					// print the moved point on the file
	  ++pointIterator;
  }
  myfile.close();
  //the "1" is added for Slicer purpose
  ros::Publisher transformation_pub = n.advertise<registration_module::transform>("Deformable_transformation", 1000);
  ros::Rate loop_rate(10);


  int count = 0;
  while (ros::ok())
  {
    registration_module::transform msg;
    const boost::array<double, 3ul> newtransform = {{registeredPoint[0], registeredPoint[1], registeredPoint[2]}};
    msg.data = newtransform;

    transformation_pub.publish(msg);

    ros::spinOnce ();
    loop_rate.sleep();
    ++count;
  }

  cout << "END" << endl;
  return 0;
}

