//LANDMARK REGISTRATION
/* INPUT: 3 *.txt files:
 fixedLandmarksFile     --> coordinates of fixed landmarks (at least 3)
 movingLandmarksFile    --> coordinates of moving landmarks (at least 3)
 movingPointFile        --> coordinates of the moving points, i.e. the points that have to be registered
 
 Point coordinates have to be separated by single space. Example:
 p0x p0y p0z
 p1x p1y p1z
 ...
 
OUTPUT: +.txt and *.fcsv files with the moving-point new (registered) coordinates*/


//ROS headers

#include "ros/ros.h"
#include "std_msgs/String.h"
// custom ROS msg to save transformation matrix as a 12 elements array i.e. 12 elements of transformation matrix
#include "registration_module/transformLandmark.h"


// VTK and C++ STL headers
#include <vtkVersion.h>
#include <vtkPoints.h>
#include <vtkSmartPointer.h>
#include <vtkLandmarkTransform.h>
#include <vtkMatrix4x4.h>
#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;

int main(int argc, char * argv[])
{
	if (argc < 4)
	{
		std::cerr << "Arguments Missing. " << std::endl;
		std::cerr <<
			"Usage:  Landmark registration   fixedLandmarksFile  movingLandmarksFile  movingPointFile"
			<< std::endl;
		return EXIT_FAILURE;
	}

    //ros::init(argc, argv, "Landmark_based_registration");

    //ros::NodeHandle n;

	//1) Read fixed-landmark coordinates from the first *.txt file

	ifstream   fixedLandmarksFile;
	fixedLandmarksFile.open(argv[1]);
	double x, y, z;
	if (fixedLandmarksFile.fail())
	{
		cerr << "Error opening points file with name : " << endl;
		cerr << argv[1] << endl;
		return EXIT_FAILURE;
	}
	vtkSmartPointer<vtkPoints> fixedLandmarks =
		vtkSmartPointer<vtkPoints>::New();
	cout << "fixed points:" << endl;
	while (fixedLandmarksFile >> x >> y >> z)
	{
		fixedLandmarks->InsertNextPoint(x, y, z);
	}


	//2) Read moving-landmark coordinates from the second *.txt file

	ifstream   movingLandmarksFile;
	movingLandmarksFile.open(argv[2]);
	if (movingLandmarksFile.fail())
	{
		cerr << "Error opening points file with name : " << endl;
		cerr << argv[2] << endl;
		return EXIT_FAILURE;
	}
	vtkSmartPointer<vtkPoints> movingLandmarks =
		vtkSmartPointer<vtkPoints>::New();
	cout << "moving landmarks:" << endl;
	while (movingLandmarksFile >> x>>y>>z)
	{
		movingLandmarks->InsertNextPoint(x,y,z);
		//cout << x << ", "<<y<<", "<<z<< endl;
	}

	//Check the size of the moving and fixed landmarks
	if (movingLandmarks->GetNumberOfPoints() != fixedLandmarks->GetNumberOfPoints())
	{
		cerr << "The fixed landmarks and the moving landmarks must be equal size" << endl;
		return EXIT_FAILURE;
	}

	if (movingLandmarks->GetNumberOfPoints() <3)
	{
		cerr << "The landmarks must be at least 3" << endl;
		return EXIT_FAILURE;
	}



	//3)  Read the moving-point coordinates from the third *.txt file
	ifstream   movingFile;
	movingFile.open(argv[3]);
	if (movingFile.fail())
	{
		cerr << "Error opening points file with name : " << endl;
		cerr << argv[3] << endl;
		return EXIT_FAILURE;
	}
	vtkSmartPointer<vtkPoints> movingPoints =
		vtkSmartPointer<vtkPoints>::New();
	cout << "moving points:" << endl;
	while (movingFile >> x >> y >> z)
	{
		movingPoints->InsertNextPoint(x, y, z);
		//cout << x << ", " << y << ", " << z << endl;
	}


	//4)  Setup the landmark transform
	vtkSmartPointer<vtkLandmarkTransform> landmarkTransform =
		vtkSmartPointer<vtkLandmarkTransform>::New();
	landmarkTransform->SetSourceLandmarks(movingLandmarks);
	landmarkTransform->SetTargetLandmarks(fixedLandmarks);
	landmarkTransform->SetModeToRigidBody();
	landmarkTransform->Update(); 

	// 6) Apply the transformation to the moving points and write the new-point coordinates on the *.txt and *.fcsv output files

	vtkSmartPointer<vtkPoints> movedPoints =
		vtkSmartPointer<vtkPoints>::New();
	landmarkTransform->TransformPoints(movingPoints, movedPoints);
	ofstream myfile;
	ofstream myfiletxt;
	myfile.open("movedPoint.fcsv");
	myfiletxt.open("movedPoint.txt");
	for (vtkIdType i = 0; i < movedPoints->GetNumberOfPoints(); i++)
	{
		double p[3];
		movedPoints->GetPoint(i, p);
		myfile << "1," << p[0] << "," << p[1] << "," << p[2] << "\n";
		myfiletxt<< p[0] << " " << p[1] << " " << p[2] << "\n";
	}
	myfile.close();
	myfiletxt.close();
	

	// 7) Display the transformation matrix that was computed
	vtkMatrix4x4* mat = landmarkTransform->GetMatrix();
	cout << "Matrix: " << *mat << std::endl;

    //ros::Publisher transformation_pub1 = n.advertise<registration_module::transformLandmark>("transformation_LandmarkRegistration", 1000);
    //ros::Rate loop_rate(10);
    // Publisher for transformation matrix

    ros::Publisher transformation_pub1 = n


    //int count = 0;
    //while (ros::ok()) {
    //    registration_module::transformLandmark msg;
    //    const boost::array<double, 12ul> newtransform = {*mat->GetData()};
    //    msg.data = newtransform;

    //    transformation_pub1.publish(msg);

    //    ros::spinOnce();
    //    loop_rate.sleep();
    //    ++count;

    //}
	return 0;

}